#!/usr/bin/env pythoh3

'''
Programa para mostrar la lista de la compra
'''

habitual = ['leche', 'pan', 'huevos']

def main():
    habitual = ["leche", "pan", "huevos", "zumo", "jamón"]
#Lista de la compra habitual
    especifica = ["pimiento", "tomate", "mantequilla"]

    elemento = input("Elemento a comprar: ")

    while elemento != "":
        if elemento not in especifica:
            especifica.append(elemento)
        elemento = input("Elemento a comprar: ")

        lista_final = habitual.copy()

        #Solo elementos no presentes en ambas listas

        for k in habitual:
            if k not in habitual:
                lista_final += [k]
        print("Lista de la compra final: ")
        for n in lista_final:
            print(n)

        print("Elementos habituales: ", len(habitual))
        print("Elementos espeíficos: ", len(especifica))
        print("Elementos en la lista: ", len(lista_final))


if __name__ == '__main__':
    main()